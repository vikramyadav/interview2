require 'rails_helper'

RSpec.describe Tag, type: :model do
  describe 'validation' do

  	describe '#title' do
	  	it 'is not valid without a title' do
	  		tag = described_class.new(title: nil)
	  		expect(tag).to_not be_valid
	  	end

	  	it 'is not valid with empty title' do
	  		tag = described_class.new(title: '')
	  		expect(tag).to_not be_valid
	  	end

	  	it 'is valid with non empty title' do
	  		tag = described_class.new(title: 'adf')
	  		expect(tag).to be_valid
	  	end
	  end
  end
end
