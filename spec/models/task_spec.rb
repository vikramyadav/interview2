require 'rails_helper'

RSpec.describe Task, type: :model do
  describe 'validation' do

  	describe '#title' do
	  	it 'is not valid without a title' do
	  		task = described_class.new(title: nil)
	  		expect(task).to_not be_valid
	  	end

	  	it 'is not valid with empty title' do
	  		task = described_class.new(title: '')
	  		expect(task).to_not be_valid
	  	end

	  	it 'is valid with non empty title' do
	  		task = described_class.new(title: 'adf')
	  		expect(task).to be_valid
	  	end
	  end
  end
end
