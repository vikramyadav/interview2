require 'rails_helper'

RSpec.describe "Tags", type: :request do
  let(:headers) do
		{'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}
	end

	describe 'getting tags' do
	
		context 'when no tags exist' do
			before do
				get api_v1_tags_path, headers: headers
				expect(response).to have_http_status(:success)
			end

			it 'returns empty array' do
				parsed_response = JSON.parse(response.body)['data']
				expect(parsed_response).to eq []
			end
		end

		context 'when tags exist' do
			let!(:tags) do
				[
					Tag.create!(title: 'Some tag'),
					Tag.create!(title: 'Some other tag')
				]
			end

			before do
				get api_v1_tags_path, headers: headers
				expect(response).to have_http_status(:success)
			end

			it 'returns all tags' do
				tags = JSON.parse(response.body)['data']
				titles = tags.map{|tag| tag&.fetch('attributes', nil)&.fetch('title', nil) }
				expect(titles).to match_array(['Some tag', 'Some other tag'])
			end
		end
	end

	describe 'creation of a tag' do
		let(:data) do
			{ data: { attributes: {title: title}  } }.to_json
		end

		context 'when request data contains a title' do
			before do
				post api_v1_tags_path, params: data, headers: headers
			end

			context 'when title is a non empty string' do
				let(:title) { 'Get Milk' }

				it 'return status 201' do
					expect(response).to have_http_status(:created)
				end

				it 'returns an id, type and title in response' do
					parsed_response = JSON.parse(response.body)['data']
					expect(parsed_response['type']).to eq 'tags'
					expect(parsed_response['id']).to be_truthy
					expect(parsed_response['attributes']['title']).to eq 'Get Milk'
				end
			end

			context 'when the title is empty string' do
				let(:title) { '' }

				it 'returns status 400' do
					expect(response).to have_http_status(:bad_request)
				end

				it 'returns an error response' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 400
					expect(parsed_response['title']).to eq 'Bad Data'
					expect(parsed_response['detail']).to eq "Validation failed: Title can't be blank"
				end
			end

			context 'when the title is not provided' do
				let(:title) { nil }

				it 'returns status 400' do
					expect(response).to have_http_status(:bad_request)
				end

				it 'returns an error response' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 400
					expect(parsed_response['title']).to eq 'Bad Data'
					expect(parsed_response['detail']).to eq "Validation failed: Title can't be blank"
				end
			end
		end
	end

	describe 'update a tag', type: :request do
		let(:data) do
			{ data: { attributes: {title: title}  } }.to_json
		end

		let(:title) { nil }

		context 'when a tag exists' do
			let!(:tag) { Tag.create!(title: 'Polish shoes') }

			before do
				patch api_v1_tag_path(tag.id), params: data, headers: headers
			end

			context 'when the title name is provided' do
				let(:title) { 'Run errand' }

				it 'returns the 200' do
					expect(response).to have_http_status(:success)
				end

				it 'returns the upated object' do
					parsed_response = JSON.parse(response.body)['data']
					expect(parsed_response['type']).to eq 'tags'
					expect(parsed_response['id']).to eq tag.id.to_s
					expect(parsed_response['attributes']['title']).to eq 'Run errand'
				end
			end

			context 'when title name is not provided' do
				let(:title) { '' }

				it 'returns status 400' do
					expect(response).to have_http_status(:bad_request)
				end

				it 'returns error message' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 400
					expect(parsed_response['title']).to eq 'Bad Data'
					expect(parsed_response['detail']).to eq "Validation failed: Title can't be blank"
				end
			end
		end

		context 'when a tag does not exist' do
			before do
				patch api_v1_tag_path('invalid-id'), params: data, headers: headers
			end

			it 'returns a status 404' do
				expect(response).to have_http_status(:not_found)
			end

			it 'returns error message' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 404
					expect(parsed_response['title']).to eq 'Not Found'
					expect(parsed_response['detail']).to eq "Couldn't find Tag with 'id'=invalid-id"
			end
		end
	end
end
