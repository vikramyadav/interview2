require 'rails_helper'

describe 'Task Management', type: :request do
	let(:headers) do
		{'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}
	end

	describe 'getting tasks' do

		context 'when no tasks exist' do
			before do
				get api_v1_tasks_path, headers: headers
				expect(response).to have_http_status(:success)
			end

			it 'returns empty array' do
				parsed_response = JSON.parse(response.body)['data']
				expect(parsed_response).to eq []
			end
		end

		context 'when tasks exist' do
			let!(:tasks) do
				[
					Task.create!(title: 'Some task'),
					Task.create!(title: 'Some other task')
				]
			end

			before do
				get api_v1_tasks_path, headers: headers
				expect(response).to have_http_status(:success)
			end

			it 'returns all tasks' do
				tasks = JSON.parse(response.body)['data']
				titles = tasks.map{|task| task&.fetch('attributes', nil)&.fetch('title', nil) }
				expect(titles).to match_array(['Some task', 'Some other task'])
			end
		end
	end

	describe 'creation of a task' do
		let(:data) do
			{ data: { attributes: {title: title}  } }.to_json
		end

		context 'when request data contains a title' do
			before do
				post api_v1_tasks_path, params: data, headers: headers
			end

			context 'when title is a non empty string' do
				let(:title) { 'Get Milk' }

				it 'return status 201' do
					expect(response).to have_http_status(:created)
				end

				it 'returns an id, type and title in response' do
					parsed_response = JSON.parse(response.body)['data']
					expect(parsed_response['type']).to eq 'tasks'
					expect(parsed_response['id']).to be_truthy
					expect(parsed_response['attributes']['title']).to eq 'Get Milk'
				end
			end

			context 'when the title is empty string' do
				let(:title) { '' }

				it 'returns status 400' do
					expect(response).to have_http_status(:bad_request)
				end

				it 'returns an error response' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 400
					expect(parsed_response['title']).to eq 'Bad Data'
					expect(parsed_response['detail']).to eq "Validation failed: Title can't be blank"
				end
			end

			context 'when the title is not provided' do
				let(:title) { nil }

				it 'returns status 400' do
					expect(response).to have_http_status(:bad_request)
				end

				it 'returns an error response' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 400
					expect(parsed_response['title']).to eq 'Bad Data'
					expect(parsed_response['detail']).to eq "Validation failed: Title can't be blank"
				end
			end
		end
	end

	describe 'update a task', type: :request do
		let(:data) do
			{ data: { attributes: {title: title}  } }.to_json
		end

		let(:title) { nil }

		context 'when a task exists' do
			let!(:task) { Task.create!(title: 'Polish shoes') }
			let!(:tag) { Tag.create!(title: 'misc') }

			before do
				patch api_v1_task_path(task.id), params: data, headers: headers
			end

			context 'when the title name is provided' do
				let(:title) { 'Run errand' }

				it 'returns the 200' do
					expect(response).to have_http_status(:success)
				end

				it 'returns the upated object' do
					parsed_response = JSON.parse(response.body)['data']
					expect(parsed_response['type']).to eq 'tasks'
					expect(parsed_response['id']).to eq task.id.to_s
					expect(parsed_response['attributes']['title']).to eq 'Run errand'
				end

				context 'when tags are provided' do
					context 'when tags exist' do
						let(:data) do
							{
								data: {
									attributes: {
										title: title,
										tags: ['misc']
									}
								}
							}.to_json
						end

						it 'returns status 200' do
							expect(response).to have_http_status(:ok)
						end

						it 'returns the updated task with tags' do
							parsed_response = JSON.parse(response.body)
							updated_tag = parsed_response['data']['relationships']['tags']['data'].first
							expected = {
								'id' => tag.id.to_s,
								'type' => 'tags'
							}

							expect(updated_tag).to eq(expected)
						end
					end

					context 'when tags do not exist' do
						let(:data) do
							{
								data: {
									attributes: {
										title: title,
										tags: ['missing']
									}
								}
							}.to_json
						end

						it 'returns bad request' do
							expect(response).to have_http_status(:not_found)
						end

						it 'returns error message' do
							parsed_response = JSON.parse(response.body)
							expect(parsed_response['status']).to eq 404
							expect(parsed_response['title']).to eq 'Not Found'
							expect(parsed_response['detail']).to eq "Couldn't find Tag"
						end
					end
				end
			end

			context 'when title name is not provided' do
				let(:title) { '' }

				it 'returns status 400' do
					expect(response).to have_http_status(:bad_request)
				end

				it 'returns error message' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 400
					expect(parsed_response['title']).to eq 'Bad Data'
					expect(parsed_response['detail']).to eq "Validation failed: Title can't be blank"
				end
			end
		end

		context 'when a task does not exist' do
			before do
				patch api_v1_task_path('invalid-id'), params: data, headers: headers
			end

			it 'returns a status 404' do
				expect(response).to have_http_status(:not_found)
			end

			it 'returns error message' do
					parsed_response = JSON.parse(response.body)
					expect(parsed_response['status']).to eq 404
					expect(parsed_response['title']).to eq 'Not Found'
					expect(parsed_response['detail']).to eq "Couldn't find Task with 'id'=invalid-id"
			end
		end
	end
end
