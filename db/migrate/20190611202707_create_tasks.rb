class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :title, null: false

      t.index :title, unique: true
      t.timestamps
    end
  end
end
