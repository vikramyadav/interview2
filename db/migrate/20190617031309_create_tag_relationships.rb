class CreateTagRelationships < ActiveRecord::Migration[5.2]
  def change
    create_table :tag_relationships do |t|
    	t.belongs_to :tag, index: true
    	t.references :taggable, index: {name: :taggable_index}, polymorphic: true

    	t.timestamps
    end
  end
end
