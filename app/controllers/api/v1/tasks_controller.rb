class Api::V1::TasksController < ApplicationController

	before_action :set_task, only: [:update]
	before_action :set_attributes

	def index
		render json: Task.all
	end

	def create
		task = Task.create!(title: @title)
		render json: task, status: :created
	rescue ActiveRecord::RecordInvalid => e
		respond_with_error(status: 400, title: 'Bad Data', detail: e.message)
	end

	def update
		if !@tags.empty?
			@task.update!(title: @title, tags: @tags)
		else
			@task.update!(title: @title)
		end

		render json: @task.reload, status: :ok
	rescue ActiveRecord::RecordInvalid => e
		respond_with_error(status: 400, title: 'Bad Data', detail: e.message)
	end

	private

	def set_task
		@task = Task.find(params[:id])

	rescue ActiveRecord::RecordNotFound => e
		respond_with_error(status: 404, title: 'Not Found', detail: e.message)
	end

	def set_attributes
		@title = params&.fetch('data', nil)&.fetch('attributes', nil)&.fetch('title', nil)
		tag_titles = params&.fetch('data', nil)&.fetch('attributes', nil)&.fetch('tags', []) || []
		@tags = tag_titles.map { |title| Tag.find_by_title!(title) }

	rescue ActiveRecord::RecordNotFound => e
		respond_with_error(status: 404, title: 'Not Found', detail: e.message)
	end
end
