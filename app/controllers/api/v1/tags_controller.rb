class Api::V1::TagsController < ApplicationController
  before_action :set_task, only: [:update]

  def index
    render json: Tag.all
  end
  
  def create
    task = Tag.create!(title: params&.fetch('data', nil)&.fetch('attributes', nil)&.fetch('title', nil))
    render json: task, status: :created
  rescue ActiveRecord::RecordInvalid => e
    respond_with_error(status: 400, title: 'Bad Data', detail: e.message)
  end

  def update
    @task.update!(title: params&.fetch('data', nil)&.fetch('attributes', nil)&.fetch('title', nil))
    render json: @task, status: :ok
  rescue ActiveRecord::RecordInvalid => e
    respond_with_error(status: 400, title: 'Bad Data', detail: e.message)
  end

  private 

  def set_task
    @task = Tag.find(params[:id])

  rescue ActiveRecord::RecordNotFound => e
    respond_with_error(status: 404, title: 'Not Found', detail: e.message)
  end
end
