# frozen_string_literal: true

class ApplicationController < ActionController::API

	private

	def respond_with_error(status:, title:, detail:)
		render status: status, json: {
			status: status,
			title: title,
			detail: detail 
		}
	end
end
