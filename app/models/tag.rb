class Tag < ApplicationRecord
	validates :title, presence: true

	has_many :tag_relationships
	has_many :tasks, through: :tag_relationships, source: :taggable, source_type: Task
end
