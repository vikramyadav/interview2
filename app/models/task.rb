class Task < ApplicationRecord
	include Taggable

	validates :title, presence: true
end
